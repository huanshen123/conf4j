package com.conf4j;

import org.slf4j.LoggerFactory;

import com.conf4j.annotation.Config;
import com.conf4j.kit.ConfKit;

/**
 * conf4j配置扫描器配置类
 * 
 * @author dingnate
 *
 */
@Config(file = "conf4j.properties", prefix = "conf4j.config.scaner")
public class Conf4jConfigScanerConf implements Conf {
	public final static Conf4jConfigScanerConf ME = new Conf4jConfigScanerConf();
	static {
		try {
			ConfKit.handler(Conf4jConfigScanerConf.class);
		} catch (Exception e) {
			LoggerFactory.getLogger(Conf4jConfigScanerConf.class).error(
					String.format("handler %s failed.", Conf4jConfigScanerConf.class.getName()), e);
		}
	}

	private String className;
	private String url;
	private String user;
	private String password;
	private long intervalSec;
	private String keys;

	/**
	 * @return the className
	 */
	public final String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public final void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the url
	 */
	public final String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public final void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the intervalSec
	 */
	public final long getIntervalSec() {
		return intervalSec;
	}

	/**
	 * @param intervalSec the intervalSec to set
	 */
	public final void setIntervalSec(long intervalSec) {
		this.intervalSec = intervalSec;
	}

	/**
	 * @return the keys
	 */
	public final String getKeys() {
		return keys;
	}

	/**
	 * @param keys the keys to set
	 */
	public final void setKeys(String keys) {
		this.keys = keys;
	}

	/**
	 * @return the user
	 */
	public final String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public final void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the password
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public final void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Conf4jConfigScanerConf [className=" + className + ", url=" + url + ", user=" + user + ", password="
				+ password + ", intervalSec=" + intervalSec + ", keys=" + keys + "]";
	}
}
