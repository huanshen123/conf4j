/**
 * 
 */
package com.conf4j.scaner;

/**
 * 配置扫描监听器
 * 
 * @author dingnate
 *
 */
public interface IConfigScanerLinstener {
	void onChange(String key, String propertyKey, String oldValue, String newValue);
}
