package com.conf4j.git.scaner;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.conf4j.Conf4jConfigScanerConf;
import com.conf4j.file.scaner.ConfigFileScaner;
import com.conf4j.git.kit.GitKit;

/**
 * Git扫描器
 * 
 * @author dingnate
 *
 */
public class ConfigGitScaner extends ConfigFileScaner {
	private static transient final Logger LOG = LoggerFactory.getLogger(ConfigGitScaner.class);

	@Override
	protected boolean scan() {
		try {
			updateLocalRepo();
		} catch (Exception e) {
			LOG.error(String.format("git pull(clone) from %s failed", Conf4jConfigScanerConf.ME.getUrl()), e);
			return false;
		}
		return super.scan();
	}

	private void updateLocalRepo() throws GitAPIException, IOException {
		GitKit.doCloneOrUpdate(
				Conf4jConfigScanerConf.ME.getUrl(),
				GitKit.getCredentialsProvider(Conf4jConfigScanerConf.ME.getUser(),
						Conf4jConfigScanerConf.ME.getPassword()), new File(getLocalPath()), null);
	}
}
