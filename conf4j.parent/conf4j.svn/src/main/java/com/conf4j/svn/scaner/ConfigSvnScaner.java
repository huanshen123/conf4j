package com.conf4j.svn.scaner;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;

import com.conf4j.Conf4jConfigScanerConf;
import com.conf4j.file.scaner.ConfigFileScaner;
import com.conf4j.svn.kit.SvnKit;

/**
 * Svn扫描器
 * 
 * @author dingnate
 *
 */
public class ConfigSvnScaner extends ConfigFileScaner {
	private static transient final Logger LOG = LoggerFactory.getLogger(ConfigSvnScaner.class);

	@Override
	protected boolean scan() {
		try {
			updateLocalRepo();
		} catch (Exception e) {
			LOG.error(String.format("svn update(chectout) from %s failed", Conf4jConfigScanerConf.ME.getUrl()), e);
			return false;
		}
		return super.scan();
	}

	private void updateLocalRepo() throws SVNException {
		SvnKit.doCheckoutOrUpdate(new File(getLocalPath()), Conf4jConfigScanerConf.ME.getUrl(),
				Conf4jConfigScanerConf.ME.getUser(), Conf4jConfigScanerConf.ME.getPassword(), SVNRevision.HEAD);
	}
}
